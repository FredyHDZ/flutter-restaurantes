import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_examen_teeb/models/Restaurant.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import '../models/Restaurant.dart';

import 'package:http/http.dart' as http;

class PageInfoRestaurant extends StatefulWidget {
  final Restaurant restaurant;
  const PageInfoRestaurant(this.restaurant, {Key key}) : super(key: key);

  @override
  _PageInfoRestaurantState createState() => _PageInfoRestaurantState();
}

class _PageInfoRestaurantState extends State<PageInfoRestaurant> {
  Future<List<Review>> reseniaList;

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    reseniaList = getReviews(this.widget.restaurant);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text(this.widget.restaurant.name),),
      body: SingleChildScrollView(
        child: Center(child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: paintImage(widget.restaurant)
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    child: RatingBar.builder(
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.teal[300],
                        ),
                        initialRating: widget.restaurant.rating,
                        itemCount: 5,
                        allowHalfRating: true,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        tapOnlyMode: true,
                        
                      )
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      widget.restaurant.description, 
                      textAlign: TextAlign.center, 
                      //style: DefaultTextStyle.of(context).style.apply(fontSizeFactor: 0.5)
                      style: TextStyle(
                        fontSize: 15,
                        foreground: Paint()
                          ..style = PaintingStyle.stroke
                          ..strokeWidth = 2
                          ..color = Colors.blue[700],
                      ),
                    ),
                  ),
                  Container(
                    height: 300,
                    padding: EdgeInsets.all(20),
                    child: FutureBuilder(
                      future: reseniaList,
                      builder: (context, snapshot){
                        if(snapshot.hasData){
                          return ListView.builder(
                            shrinkWrap: true,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index){
                              return Card(
                                child: ListTile(
                                  onTap: (){mostrarAlerta(snapshot.data[index], context);},
                                  title: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(snapshot.data[index].email + "  " + snapshot.data[index].created),
                                  ),
                                  leading: CircleAvatar(
                                    radius: 50,
                                    backgroundColor: Colors.transparent,
                                    child: Text(snapshot.data[index].comments),
                                  ),
                                )
                              );
                            }
                          );
                        }
                        return Center(child: CircularProgressIndicator(),);
                        
                      }
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextButton(
                      onPressed: (){
                        sendResenia(widget.restaurant, context);
                        reseniaList = getReviews(this.widget.restaurant);
                        
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text("Agregar Reseña"),
                      ),
                      style: ButtonStyle(
                        alignment: Alignment.center,
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(Colors.teal[300]),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.teal[300])
                          )
                        )
                      ),
                    )
                  )
                ],
              ),
            )
        ),
    );
  }

  paintImage(Restaurant item){
    if(item.logo != null && item.logo != ""){
      return Image.network(item.logo, fit: BoxFit.fill,);
    }else{
      return Text("");
    }
  }

  sendResenia(Restaurant restaurant, BuildContext context){
    TextEditingController textFieldR = TextEditingController(text: "");
    TextEditingController textFieldE = TextEditingController(text: "");
    double ratingR = 0.0;
    showDialog(
      context: context, 
      builder: (context){
        return AlertDialog(
          title: Text("Escribe aquí tus comentarios"),
          content: Container(
            child: Wrap( children: <Widget> [SizedBox(
              //height: 110,
              child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: TextFormField(
                        controller: textFieldE,
                        decoration: const InputDecoration(
                          hintText: 'Escribe tu Email:',
                        ),
                        /**/ 
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: TextFormField(
                        controller: textFieldR,
                        decoration: const InputDecoration(
                          hintText: 'Escribe tu reseña aquí',
                        ),
                        /**/ 
                      ),
                    ),
                    RatingBar.builder(
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.cyanAccent[700],
                      ),
                      initialRating: 0,
                      itemCount: 5,
                      allowHalfRating: true,
                      itemPadding: EdgeInsets.symmetric(horizontal: 5.0),
                      tapOnlyMode: false,
                      direction: Axis.horizontal,
                      onRatingUpdate: (value) {
                        ratingR = value;
                      },
                    )
                  ]
              )
            )]
            )
          ),
          actions: [

              Center(
                child: TextButton(
                  style: ButtonStyle(
                    alignment: Alignment.center,
                    foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.teal[300]),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.teal[300])
                      )
                    )
                  ),
                  onPressed: (){
                    var enviado = postResenia(textFieldR.text, textFieldE.text, ratingR, restaurant);
                    
                    Navigator.pop(context);
                  }, 
                  child: Text("Enviar"),  
                ),

              )
          ],
        );
      }
    );
  }

  Future<List<Review>> getReviews(Restaurant restaurant) async{
    final response = await http.get("https://tellurium.behuns.com/api/reviews/");

    List<Review> reviews = [];
    if(response.statusCode == 200){ // Recibió información sin errores
      String decodeResponse = utf8.decode(response.bodyBytes); // Codificar a UTF8
      final jsonData = jsonDecode(decodeResponse);
      for(var item in jsonData){
        if(item["restaurant"] == restaurant.slug){
          reviews.add(Review(item["slug"], item["restaurant"], item["email"], item["comments"], item["rating"], item["created"]));
        }
      }
      this.setState(() {
                          
      });

      return reviews;
    }else{
      throw Exception("Error: " + response.statusCode.toString());
    }
  } 
  
  Future<http.Response> postResenia(String textField, String textFieldE, double ratingR, Restaurant restaurant) async{
    print(ratingR);
    
    final response = await http.post("https://tellurium.behuns.com/api/reviews/",
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'restaurant': restaurant.slug,
      'email': textFieldE,
      'comments': textField,
      'rating': ratingR.toInt().toString()
    }
    ));

    if (response.statusCode == 201) {
      print(response.statusCode);
      reseniaList = getReviews(restaurant);
    } else {
      print(response.statusCode);
      print(response.body);
      throw Exception('Fallo al enviar la reseña');
    }
  }

  mostrarAlerta(Review review, BuildContext context){
    showDialog(
      context: context, 
      builder: (context) {
        return AlertDialog(
          title: Text(review.email + " dice:"),
          content: Container(
            child: Wrap( children: <Widget> [SizedBox(
              //height: 110,
              child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(review.comments),
                    ),
                    RatingBar.builder(
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.cyanAccent[700],
                      ),
                      initialRating: review.rating,
                      itemCount: 5,
                      allowHalfRating: true,
                      itemPadding: EdgeInsets.symmetric(horizontal: 5.0),
                      tapOnlyMode: true,
                      direction: Axis.horizontal,
                    )
                  ],
                ),
              
            )])
          ),
          actions: [
            TextButton(
              onPressed: (){
                Navigator.pop(context);
              }, 
              child: Text("Cerrar")
            )
          ],
        );
      },
      );
  }
}