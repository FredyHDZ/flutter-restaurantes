class Restaurant{
  String slug;
  String name;
  String description;
  String logo;
  double rating;
  List<String> foodType;
  List<Review> reviews;

  Restaurant(slug, name, description, logo, rating, foodType, reviews){
    this.slug = slug;
    this.name = name;
    this.description = description;
    this.logo = logo;
    if(rating != null){
      this.rating = rating;
    }else{
      this.rating = 0.0;
    }
    
    this.foodType = foodType;
    this.reviews = reviews;
  }

}

class FoodType{
  String slug;
  String name;

  FoodType(slug, name){
    this.slug = slug;
    this.name = name;
  }
}

class Review {
    String slug;
    String restaurant;
    String email;
    String comments;
    double rating;
    String created;

    Review(slug, restaurant, email, comments, rating, created){
      this.slug = slug;
      this.restaurant = restaurant;
      this.email = email;
      this.comments = comments;
      if(rating != null){
        this.rating = rating.toDouble();
      }else{
        this.rating = 0.0;
      }
      this.created = created;
    }
}