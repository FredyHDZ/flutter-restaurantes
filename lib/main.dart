import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_examen_teeb/pages/PageInfoRestaurant.dart';
import 'models/Restaurant.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';

void main() => runApp(MaterialApp(home: MyApp()));

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Future<File> _image;
  final picker = ImagePicker();

  _imgFromCamera() async {
    final File image = await ImagePicker.pickImage(
      source: ImageSource.camera, imageQuality: 50
    );
    return image;
  }

  Future<File> _imgFromGallery() async {
    File image = await  ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50
    );
    return image;
  }
  Future<List<Restaurant>> restaurantList;
  Future<List<FoodType>> foodTypeList;

  Future<List<Restaurant>> getRestaurants(String filtro) async{
    final response = await http.get("https://tellurium.behuns.com/api/restaurants/");

    List<Restaurant> restaurants = [];
    
    if(response.statusCode == 200){ // Recibió información sin errores
      String decodeResponse = utf8.decode(response.bodyBytes); // Codificar a UTF8
      final jsonData = jsonDecode(decodeResponse);
      for(var item in jsonData){
        List<String> foodType = [];
        List<Review> reviews = [];
        for(var fType in item["food_type"]){
          foodType.add(fType);
        }
        for(var rev in item["reviews"]){
          reviews.add(Review(rev["slug"], rev["restaurant"], rev["email"], rev["comments"], rev["rating"], rev["created"]));
        }
        Restaurant value = Restaurant(item["slug"], item["name"], item["description"], item["logo"], item["rating"], foodType, reviews);
        if(foodType .contains(filtro) || filtro == ""){
          restaurants.add(value);
        }
        
      }

      return restaurants;
      this.setState(() {
        
      });
    }else{
      throw Exception("Error: " + response.statusCode.toString());
    }
  }

  Future<List<FoodType>> getFoodTypes() async{
    final response = await http.get("https://tellurium.behuns.com/api/food_types/");

    List<FoodType> types = [];
    

    if(response.statusCode == 200){
      String decodeResponse = utf8.decode(response.bodyBytes); // Codificar a UTF8
      final jsonData = jsonDecode(decodeResponse);
      types.add(FoodType("", "All Types"));
      for(var item in jsonData){
        types.add(FoodType(item["slug"], item["name"]));
      }

      return types;
    }else{
      throw Exception("Error: " + response.statusCode.toString());
    }
  }

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    restaurantList = getRestaurants("");
    foodTypeList = getFoodTypes();
  }
  
  @override
  Widget build(BuildContext context) {
    String selectedFc;
    return MaterialApp(
      title: 'Main',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Restaurantes Examen'),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  child: FutureBuilder(
                    future: foodTypeList,
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        return DropdownButton<String>(
                          hint: Text("Select"),
                          value: selectedFc,
                          onChanged: (newValue) {
                            restaurantList = getRestaurants(newValue);
                            setState(() {
                              selectedFc = newValue;
                            });
                          },
                          items: snapshot.data.map<DropdownMenuItem<String>>((fc) =>
                            DropdownMenuItem<String>(
                              child: Text(fc.name),
                              value: fc.slug,
                            )
                          ).toList()
                        );
                      }
                      return Text("Cargando...");
                    },
                  ) 
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextButton(
                      onPressed: (){
                        print("PRESIONADOOOO");
                        openFormAddRestaurant(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text("Agregar Restaurante"),
                      ),
                      style: ButtonStyle(
                        alignment: Alignment.center,
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(Colors.teal[300]),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.teal[300])
                          )
                        )
                      ),
                    )
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  height: 600,
                  child:FutureBuilder(
                    future: restaurantList,
                    builder: (context, snapshot){
                      if(snapshot.hasData){
                        return ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index){
                            return Card(
                              child: ListTile(
                                onTap: (){
                                  openRestaurant(snapshot.data[index], context);
                                },
                                minVerticalPadding: 30,
                                title: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(snapshot.data[index].name),
                                ),
                                leading: CircleAvatar(
                                  radius: 50,
                                  backgroundColor: Colors.blue[900],
                                  child: paintImage(snapshot.data[index])
                                ),
                                trailing: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Icon(Icons.arrow_forward_ios),
                                ),//Icon(Icons.arrow_forward_ios),
                              )
                            );
                          }
                        );
                      }else if(snapshot.hasError){
                        return Text("Error al recuperar información");
                      }
                      return Center(child: CircularProgressIndicator(),);
                    },
                  )
                  //)])
                )
              ]
            )
          )
        )
      ),
    );
  }

  openFormAddRestaurant(BuildContext context){
    TextEditingController textName = TextEditingController(text: "");
    TextEditingController textDescription = TextEditingController(text: "");
    String selectedFc;
    File img;
    showDialog(
      context: context, 
      builder: (BuildContext context){
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: Text("Agregar Nuevo Restaurante"),
              content: Container(
                child: Wrap( children: <Widget> [SizedBox(
                  //height: 110,
                  child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: TextFormField(
                            controller: textName,
                            decoration: const InputDecoration(
                              hintText: 'Escribe el nombre del restaurante',
                            ),
                            /**/ 
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: TextFormField(
                            controller: textDescription,
                            decoration: const InputDecoration(
                              hintText: 'Descripción del restaurante',
                            ),
                            /**/ 
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: FutureBuilder(
                                  future: foodTypeList,
                                  builder: (context, snapshot){
                                    if(snapshot.hasData){
                                      return DropdownButton<String>(
                                        hint: Text("Tipo de comida"),
                                        value: selectedFc,
                                        onChanged: (newValue) {
                                          setState(() {
                                            selectedFc = newValue;
                                          });
                                        },
                                        items: snapshot.data.map<DropdownMenuItem<String>>((fc) =>
                                          DropdownMenuItem<String>(
                                            child: Text(fc.name),
                                            value: fc.slug,
                                          )
                                        ).toList()
                                      );
                                    }
                                    return Center(child: CircularProgressIndicator(),);
                                  },
                                ) 
                        ),
                        Container(
                          child: FloatingActionButton(
                            onPressed: (){
                              _image = _imgFromGallery();
                              setState(() {
                              });
                            },
                            tooltip: 'Selecciona archivo',
                            child: Icon(Icons.add_a_photo)

                          )
                        ),
                        Container(
                          child: FutureBuilder(
                            future: _image,
                            builder: (context, snapshot){
                              print(snapshot);
                              if(snapshot.hasData){
                                print(snapshot.data);
                                img = snapshot.data;
                                return Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(snapshot.data.path),
                                  );
                              }else if(snapshot.hasError){
                                return Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("Error al recuperar imagen"),
                                  );
                              }
                              return Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("seleccionar Imagen..."),
                                  );
                            },
                          )
                        )
                      ]
                  ),
                )])
              ),
              actions: [

                  Center(
                    child: TextButton(
                      style: ButtonStyle(
                        alignment: Alignment.center,
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(Colors.teal[300]),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.teal[300])
                          )
                        )
                      ),
                      onPressed: (){
                        //sendNewRestaurant(textName.text, textDescription.text, selectedFc);
                        sendNewRestaurant(textName.text, textDescription.text, selectedFc, img);
                        Navigator.pop(context);
                      }, 
                      child: Text("Agregar"),  
                    ),

                  )
              ],
            );
          }
        );
      }
    );
  }

  Future<http.Response> sendNewRestaurant(textName, textDescription, selectedFc, File img) async{
    /*var request = http.MultipartRequest('POST', Uri.parse("https://tellurium.behuns.com/api/restaurants/"));
    request.files.add(
      http.MultipartFile.fromBytes(
        'logo',
        img.readAsBytesSync(),
        filename: img.path.split("/").last
      )
    );*/
    
    var listFood = [];
    listFood.add(selectedFc);
    if(img != null){
      var request = http.MultipartRequest('POST', Uri.parse('https://tellurium.behuns.com/api/restaurants/'));
      request.fields.addAll({
        'name': textName,
        'description': textDescription,
        'food_type': selectedFc
      });
      request.files.add(await http.MultipartFile.fromPath('logo', img.path));

      http.StreamedResponse response = await request.send();
      print(response.statusCode);
      if (response.statusCode == 200 || response.statusCode == 201) {
        print(await response.stream.bytesToString());
        print(response.statusCode);
        restaurantList = getRestaurants("");
        this.setState(() {
          
        });
      }
      else {
        print(response.reasonPhrase);
      }
    }else{
        final response = await http.post("https://tellurium.behuns.com/api/restaurants/",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          'name': textName,
          'description': textDescription,
          'food_type': listFood
          //fromFile(img.path, filename:"fileName.jpg"),
        }
        ));
        if (response.statusCode == 201) {
          print(response.statusCode);
          restaurantList = getRestaurants("");
          this.setState(() {
            
          });
        } else {
          print(response.body);
          print("NOOOO");
          throw Exception('Fallo al enviar la reseña');
      }
    }
  }

  paintImage(Restaurant item){
    if(item.logo != null && item.logo != ""){
      return Image.network(item.logo, fit: BoxFit.fill,);
    }else{
      return Text(item.name.substring(0,1));
    }
  }

  openRestaurant(Restaurant restaurant, context){
    Navigator.push(context, MaterialPageRoute(
      builder: (context) => PageInfoRestaurant(restaurant)
    ));
    print("RATING: " + restaurant.rating.toString());
  }
}
